﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labControlHot
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            button1.MouseEnter += ButtonAll_MouseEnter;
            button2.MouseEnter += ButtonAll_MouseEnter;
            button3.MouseEnter += ButtonAll_MouseEnter;
            button4.MouseEnter += ButtonAll_MouseEnter;

            button1.MouseLeave += ButtonAll_MouseLeave;
            button2.MouseLeave += ButtonAll_MouseLeave;
            button3.MouseLeave += ButtonAll_MouseLeave;
            button4.MouseLeave += ButtonAll_MouseLeave;

            textBox1.MouseEnter += TextBoxAll_MouseEnter;
            textBox2.MouseEnter += TextBoxAll_MouseEnter;
            textBox3.MouseEnter += TextBoxAll_MouseEnter;

            textBox1.MouseLeave += TextBoxAll_MouseLeave;
            textBox2.MouseLeave += TextBoxAll_MouseLeave;
            textBox3.MouseLeave += TextBoxAll_MouseLeave;

            label1.MouseHover += LabelAll_MouseHover;
            label2.MouseHover += LabelAll_MouseHover;
            label3.MouseHover += LabelAll_MouseHover;

            label1.MouseLeave += LabelAll_MouseLeave;
            label2.MouseLeave += LabelAll_MouseLeave;
            label3.MouseLeave += LabelAll_MouseLeave;

        }

        private void LabelAll_MouseLeave(object sender, EventArgs e)
        {
            if (sender is Control x)
            {
                x.Font = new Font(x.Font, FontStyle.Regular);
            }
        }

        private void LabelAll_MouseHover(object sender, EventArgs e)
        {
            if (sender is Control x)
            {
                x.Font = new Font(x.Font, FontStyle.Bold);
            }
        }


        private void TextBoxAll_MouseLeave(object sender, EventArgs e)
        {
            if (sender is Control x)
            {
                x.BackColor = SystemColors.Window;
            }
        }

        private void TextBoxAll_MouseEnter(object sender, EventArgs e)
        {
            if (sender is Control x)
            {
                x.BackColor = Color.LightCyan;
            }
        }

        private void ButtonAll_MouseLeave(object sender, EventArgs e)
        {
            if (sender is Control x)
            {
                x.Margin = new Padding(3);
            }
        }

        private void ButtonAll_MouseEnter(object sender, EventArgs e)
        {
            if (sender is Control x)
            {
                x.Margin = new Padding(0);
            }
        }
    }
}
