﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.DirectoryServices.ActiveDirectory;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labImageSelectInGrid
{
    public partial class Form1 : Form
    {
        private const int minRows = 2;
        private const int maxRows = 15;
        private const int minCols = 2;
        private const int maxCols = 15;
        private readonly Bitmap b;
        private readonly Graphics g;
        private int cellWidth;
        private int cellHeight;
        private int curRow;
        private int curCol;
        private (int row, int col) selBegin;
        private (int row, int col) selEnd;

        public int Cols { get; private set; } = 5;
        public int Rows { get; private set; } = 4;

        public Form1()
        {
            InitializeComponent();

            b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            g = Graphics.FromImage(b);

            this.DoubleBuffered = true;
            this.Paint += (s, e) => e.Graphics.DrawImage(b, 0, 0);
            this.Resize += (s, e) => ResizeCells();
            this.MouseMove += Form1_MouseMove;
            this.MouseDown += Form1_MouseDown;
            this.KeyDown += Form1_KeyDown;

            ResizeCells();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F5:
                    if (Rows > minRows)
                    {
                        Rows--;
                        ResizeCells();
                    }
                    break;
                case Keys.F6:
                    if (Rows < maxRows)
                    {
                        Rows++;
                        ResizeCells();
                    }
                    break;
                case Keys.F7:
                    if (Cols > minCols)
                    {
                        Cols--;
                        ResizeCells();
                    }
                    break;
                case Keys.F8:
                    if (Cols < maxCols)
                    {
                        Cols++;
                        ResizeCells();
                    }
                    break;

            }
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                selStatBegin = selBegin = selEnd = (curRow, curCol);
                DrawCells();
            }
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            for (int i = 0; i < Rows; curRow = i, i++)            
                if (i * cellHeight > e.Y) break;
            for (int i = 0; i < Cols; curCol = i, i++)
                if (i * cellWidth > e.X) break;
            
            if (e.Button == MouseButtons.Left)
            {
                if (curRow < selBegin.row || curCol < selBegin.col)
                {
                    selBegin = (curRow, curCol);
                }
                else
                {
                    selEnd = (curRow, curCol);  
                }
            }

            DrawCells();
            this.Text = $"{selBegin.row}:{selBegin.col} - {selEnd.row}:{selEnd.col}";
        }

        private void ResizeCells()
        {
            cellWidth = this.ClientSize.Width / Cols;
            cellHeight = this.ClientSize.Height / Rows;
            DrawCells();
        }

        private void DrawCells()
        {
            //g.Clear(SystemColors.Control);
            g.Clear(DefaultBackColor);

            for (int i = 0; i <= Rows; i++)
                g.DrawLine(new Pen(Color.Black, 1), 0, i * cellHeight, Cols * cellWidth, i * cellHeight);
            for (int i = 0; i <= Cols; i++)
                g.DrawLine(new Pen(Color.Black, 1), i * cellWidth, 0, i * cellWidth, Rows * cellHeight);

            g.DrawRectangle(new Pen(Color.Red, 2),
                curCol * cellWidth, curRow * cellHeight, cellWidth, cellHeight);
            g.DrawString($"[{curRow}:{curCol}]", new Font("", 30), new SolidBrush(Color.Red),
                curCol * cellWidth, curRow * cellHeight);

            g.FillRectangle(new SolidBrush(Color.Yellow),
               selBegin.col * cellWidth, selBegin.row * cellHeight,
               (selEnd.col - selBegin.col + 1) * cellWidth,
               (selEnd.row - selBegin.row + 1) * cellHeight);


            this.Invalidate();
        }
    }
}


//  HW:

//  выделять снизу вверх
//  по правой кнопке мыши двигать все изображение
