﻿namespace labPaint
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pxImage = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pxRightColor1 = new System.Windows.Forms.PictureBox();
            this.pxRightColor2 = new System.Windows.Forms.PictureBox();
            this.pxRightColor3 = new System.Windows.Forms.PictureBox();
            this.pxRightColor4 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.buStars = new System.Windows.Forms.Button();
            this.buLoad = new System.Windows.Forms.Button();
            this.buSave = new System.Windows.Forms.Button();
            this.buImageClear = new System.Windows.Forms.Button();
            this.trPenWidth = new System.Windows.Forms.TrackBar();
            this.pxLeftColor4 = new System.Windows.Forms.PictureBox();
            this.pxLeftColor3 = new System.Windows.Forms.PictureBox();
            this.pxLeftColor2 = new System.Windows.Forms.PictureBox();
            this.pxLeftColor1 = new System.Windows.Forms.PictureBox();
            this.buRubber = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pxImage)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pxRightColor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxRightColor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxRightColor3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxRightColor4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trPenWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxLeftColor4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxLeftColor3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxLeftColor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxLeftColor1)).BeginInit();
            this.SuspendLayout();
            // 
            // pxImage
            // 
            this.pxImage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pxImage.BackColor = System.Drawing.SystemColors.Window;
            this.pxImage.Location = new System.Drawing.Point(159, 12);
            this.pxImage.Name = "pxImage";
            this.pxImage.Size = new System.Drawing.Size(629, 426);
            this.pxImage.TabIndex = 0;
            this.pxImage.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel1.BackColor = System.Drawing.SystemColors.Window;
            this.panel1.Controls.Add(this.buRubber);
            this.panel1.Controls.Add(this.pxRightColor1);
            this.panel1.Controls.Add(this.pxRightColor2);
            this.panel1.Controls.Add(this.pxRightColor3);
            this.panel1.Controls.Add(this.pxRightColor4);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.buStars);
            this.panel1.Controls.Add(this.buLoad);
            this.panel1.Controls.Add(this.buSave);
            this.panel1.Controls.Add(this.buImageClear);
            this.panel1.Controls.Add(this.trPenWidth);
            this.panel1.Controls.Add(this.pxLeftColor4);
            this.panel1.Controls.Add(this.pxLeftColor3);
            this.panel1.Controls.Add(this.pxLeftColor2);
            this.panel1.Controls.Add(this.pxLeftColor1);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(141, 426);
            this.panel1.TabIndex = 1;
            // 
            // pxRightColor1
            // 
            this.pxRightColor1.BackColor = System.Drawing.Color.Red;
            this.pxRightColor1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pxRightColor1.Location = new System.Drawing.Point(15, 94);
            this.pxRightColor1.Name = "pxRightColor1";
            this.pxRightColor1.Size = new System.Drawing.Size(20, 20);
            this.pxRightColor1.TabIndex = 0;
            this.pxRightColor1.TabStop = false;
            // 
            // pxRightColor2
            // 
            this.pxRightColor2.BackColor = System.Drawing.Color.Blue;
            this.pxRightColor2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pxRightColor2.Location = new System.Drawing.Point(45, 94);
            this.pxRightColor2.Name = "pxRightColor2";
            this.pxRightColor2.Size = new System.Drawing.Size(20, 20);
            this.pxRightColor2.TabIndex = 0;
            this.pxRightColor2.TabStop = false;
            // 
            // pxRightColor3
            // 
            this.pxRightColor3.BackColor = System.Drawing.Color.Pink;
            this.pxRightColor3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pxRightColor3.Location = new System.Drawing.Point(75, 94);
            this.pxRightColor3.Name = "pxRightColor3";
            this.pxRightColor3.Size = new System.Drawing.Size(20, 20);
            this.pxRightColor3.TabIndex = 0;
            this.pxRightColor3.TabStop = false;
            // 
            // pxRightColor4
            // 
            this.pxRightColor4.BackColor = System.Drawing.Color.Yellow;
            this.pxRightColor4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pxRightColor4.Location = new System.Drawing.Point(105, 94);
            this.pxRightColor4.Name = "pxRightColor4";
            this.pxRightColor4.Size = new System.Drawing.Size(20, 20);
            this.pxRightColor4.TabIndex = 0;
            this.pxRightColor4.TabStop = false;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(15, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 23);
            this.label2.TabIndex = 6;
            this.label2.Text = "ПКМ";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(15, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 23);
            this.label1.TabIndex = 6;
            this.label1.Text = "ЛКМ";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buStars
            // 
            this.buStars.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buStars.Location = new System.Drawing.Point(15, 296);
            this.buStars.Name = "buStars";
            this.buStars.Size = new System.Drawing.Size(110, 23);
            this.buStars.TabIndex = 5;
            this.buStars.Text = "Звездочки";
            this.buStars.UseVisualStyleBackColor = true;
            // 
            // buLoad
            // 
            this.buLoad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buLoad.Location = new System.Drawing.Point(15, 327);
            this.buLoad.Name = "buLoad";
            this.buLoad.Size = new System.Drawing.Size(110, 23);
            this.buLoad.TabIndex = 4;
            this.buLoad.Text = "Загрузить";
            this.buLoad.UseVisualStyleBackColor = true;
            // 
            // buSave
            // 
            this.buSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buSave.Location = new System.Drawing.Point(15, 359);
            this.buSave.Name = "buSave";
            this.buSave.Size = new System.Drawing.Size(110, 23);
            this.buSave.TabIndex = 3;
            this.buSave.Text = "Сохранить";
            this.buSave.UseVisualStyleBackColor = true;
            // 
            // buImageClear
            // 
            this.buImageClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buImageClear.Location = new System.Drawing.Point(15, 388);
            this.buImageClear.Name = "buImageClear";
            this.buImageClear.Size = new System.Drawing.Size(110, 23);
            this.buImageClear.TabIndex = 2;
            this.buImageClear.Text = "Очистить";
            this.buImageClear.UseVisualStyleBackColor = true;
            // 
            // trPenWidth
            // 
            this.trPenWidth.Location = new System.Drawing.Point(15, 131);
            this.trPenWidth.Name = "trPenWidth";
            this.trPenWidth.Size = new System.Drawing.Size(110, 45);
            this.trPenWidth.TabIndex = 1;
            // 
            // pxLeftColor4
            // 
            this.pxLeftColor4.BackColor = System.Drawing.Color.Yellow;
            this.pxLeftColor4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pxLeftColor4.Location = new System.Drawing.Point(105, 35);
            this.pxLeftColor4.Name = "pxLeftColor4";
            this.pxLeftColor4.Size = new System.Drawing.Size(20, 20);
            this.pxLeftColor4.TabIndex = 0;
            this.pxLeftColor4.TabStop = false;
            // 
            // pxLeftColor3
            // 
            this.pxLeftColor3.BackColor = System.Drawing.Color.Pink;
            this.pxLeftColor3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pxLeftColor3.Location = new System.Drawing.Point(75, 35);
            this.pxLeftColor3.Name = "pxLeftColor3";
            this.pxLeftColor3.Size = new System.Drawing.Size(20, 20);
            this.pxLeftColor3.TabIndex = 0;
            this.pxLeftColor3.TabStop = false;
            // 
            // pxLeftColor2
            // 
            this.pxLeftColor2.BackColor = System.Drawing.Color.Blue;
            this.pxLeftColor2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pxLeftColor2.Location = new System.Drawing.Point(45, 35);
            this.pxLeftColor2.Name = "pxLeftColor2";
            this.pxLeftColor2.Size = new System.Drawing.Size(20, 20);
            this.pxLeftColor2.TabIndex = 0;
            this.pxLeftColor2.TabStop = false;
            // 
            // pxLeftColor1
            // 
            this.pxLeftColor1.BackColor = System.Drawing.Color.Red;
            this.pxLeftColor1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pxLeftColor1.Location = new System.Drawing.Point(15, 35);
            this.pxLeftColor1.Name = "pxLeftColor1";
            this.pxLeftColor1.Size = new System.Drawing.Size(20, 20);
            this.pxLeftColor1.TabIndex = 0;
            this.pxLeftColor1.TabStop = false;
            // 
            // buRubber
            // 
            this.buRubber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buRubber.Location = new System.Drawing.Point(15, 264);
            this.buRubber.Name = "buRubber";
            this.buRubber.Size = new System.Drawing.Size(110, 23);
            this.buRubber.TabIndex = 5;
            this.buRubber.Text = "Ластик";
            this.buRubber.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pxImage);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pxImage)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pxRightColor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxRightColor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxRightColor3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxRightColor4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trPenWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxLeftColor4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxLeftColor3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxLeftColor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxLeftColor1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pxImage;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pxColor4;
        private System.Windows.Forms.PictureBox pxColor3;
        private System.Windows.Forms.PictureBox pxColor2;
        private System.Windows.Forms.PictureBox pxColor1;
        private System.Windows.Forms.TrackBar trPenWidth;
        private System.Windows.Forms.Button buImageClear;
        private System.Windows.Forms.Button buStars;
        private System.Windows.Forms.Button buLoad;
        private System.Windows.Forms.Button buSave;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pxRightColor1;
        private System.Windows.Forms.PictureBox pxRightColor2;
        private System.Windows.Forms.PictureBox pxRightColor3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pxLeftColor4;
        private System.Windows.Forms.PictureBox pxLeftColor3;
        private System.Windows.Forms.PictureBox pxLeftColor2;
        private System.Windows.Forms.PictureBox pxLeftColor1;
        private System.Windows.Forms.PictureBox pxRightColor4;
        private System.Windows.Forms.Button buRubber;
    }
}

