﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labPaint
{
    public partial class Form1 : Form
    {
        private readonly Bitmap b;
        private readonly Graphics g;
        private Pen PenLeft;
        private Pen PenRight;
        private Point startPoint;

        public Form1()
        {
            InitializeComponent();

            b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            g = Graphics.FromImage(b);
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            PenLeft = new Pen(Color.Red, 3);
            PenLeft.StartCap = PenLeft.EndCap = System.Drawing.Drawing2D.LineCap.Round;
            trPenWidth.Value = Convert.ToInt32(PenLeft.Width);
            trPenWidth.ValueChanged += (s, e) => PenLeft.Width = trPenWidth.Value;

            PenRight = new Pen(Color.Blue, 3);
            PenRight.StartCap = PenRight.EndCap = System.Drawing.Drawing2D.LineCap.Round;
            trPenWidth.Value = Convert.ToInt32(PenRight.Width);
            trPenWidth.ValueChanged += (s, e) => PenRight.Width = trPenWidth.Value;


            pxImage.MouseDown += (s, e) => startPoint = e.Location;
            pxImage.MouseMove += PxImage_MouseMove;
            pxImage.Paint += (s, e) => e.Graphics.DrawImage(b, 0, 0);

           

            

            pxLeftColor1.MouseDown += PxColorLeft_MouseDown;
            pxLeftColor2.MouseDown += PxColorLeft_MouseDown;
            pxLeftColor3.MouseDown += PxColorLeft_MouseDown;
            pxLeftColor4.MouseDown += PxColorLeft_MouseDown;
            pxRightColor1.MouseDown += PxColorRight_MouseDown;
            pxRightColor2.MouseDown += PxColorRight_MouseDown;
            pxRightColor3.MouseDown += PxColorRight_MouseDown;
            pxRightColor4.MouseDown += PxColorRight_MouseDown;



            buSave.Click += BuSave_Click;
            buLoad.Click += BuLoad_Click;
            buStars.Click += BuStars_Click;
            buRubber.Click += BuRubber_Click;
            buImageClear.Click += delegate
            {
                g.Clear(SystemColors.Control);
                pxImage.Invalidate();
            };
        }

        private void BuRubber_Click(object sender, EventArgs e)
        {
            PenLeft.Color = pxImage.BackColor;
        }

        private void BuStars_Click(object sender, EventArgs e)
        {
            var rnd = new Random();
            for (int i = 0; i < 200; i++)
            {
                g.FillEllipse(new SolidBrush(Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256))),
                        rnd.Next(b.Width),
                        rnd.Next(b.Height),
                        rnd.Next(1,10),
                        rnd.Next(1, 10)
                    );
                pxImage.Invalidate();
            }
        }

        private void BuLoad_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Image Files(*.BMP; *.JPG; *.GIF)| *.BMP; *.JPG; *.GIF | All files(*.*) | *.*";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                g.Clear(SystemColors.Control);
                g.DrawImage(Bitmap.FromFile(dialog.FileName), 0, 0);
                pxImage.Invalidate();
            }
        }

        private void BuSave_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "JPEG Files(*.JPG)|*.JPG;";
            if(dialog.ShowDialog() == DialogResult.OK)
            {
                b.Save(dialog.FileName);
            }
        }

        private void PxColorLeft_MouseDown(object sender, MouseEventArgs e)
        {
            if (sender is PictureBox p)
            {
                PenLeft.Color = p.BackColor;
            }
            if (e.Button == MouseButtons.Right)
            {
                if (sender is PictureBox x)
                {
                    ColorDialog colorDialog = new ColorDialog();
                    colorDialog.Color = x.BackColor;
                    if (colorDialog.ShowDialog() == DialogResult.OK)
                    {
                        x.BackColor = colorDialog.Color;
                        PenLeft.Color = x.BackColor;
                    }
                }
            }
        }

        private void PxColorRight_MouseDown(object sender, MouseEventArgs e)
        {
            if (sender is PictureBox p)
            {
                PenRight.Color = p.BackColor;
            }
            if (e.Button == MouseButtons.Right)
            {
                if (sender is PictureBox x)
                {
                    ColorDialog colorDialog = new ColorDialog();
                    colorDialog.Color = x.BackColor;
                    if (colorDialog.ShowDialog() == DialogResult.OK)
                    {
                        x.BackColor = colorDialog.Color;
                        PenRight.Color = x.BackColor;
                    }
                }
            }
        }

        private void PxImage_MouseMove (object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                g.DrawLine(PenLeft, startPoint, e.Location);
                startPoint = e.Location;
                pxImage.Invalidate();

            }
            if (e.Button == MouseButtons.Right)
            {
                g.DrawLine(PenRight, startPoint, e.Location);
                startPoint = e.Location;
                pxImage.Invalidate();

            }
        }
    }
}