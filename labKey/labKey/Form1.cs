﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labKey
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            this.KeyDown += Form1_KeyDown;
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Left:
                    laText.Text = "Left";
                    break;
                case Keys.Up:
                    laText.Text = "Up";
                    break;
                case Keys.Down:
                    laText.Text = "Down";
                    break;
                case Keys.Right:
                    laText.Text = "Right";
                    break;
                case Keys.Space:
                    if (e.Shift)
                    {
                        laText.Text = "Shift + Space";
                    }
                    else
                    {
                        laText.Text = "Space";
                    }
                    break;
                case Keys.X:
                    laText.Text = e.Shift ? "Shift + X" : "X";
                    break;
                default:
                    laText.Text = $" другая клавиша:  {e.KeyCode}";
                    break;
            }
        }
    }
}
