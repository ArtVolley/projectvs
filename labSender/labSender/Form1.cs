﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labSender
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            button1.Click += Button1_Click;
            button2.Click += Button1_Click;
            button3.Click += Button1_Click;
            label1.Click += Button1_Click;
            label2.Click += Button1_Click;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            // button1.Text = "Click";

            //if (sender is Control)
            //{
            //    ((Control)sender).Text = "Click";
            //}

            if (sender is Control x)
            {
                x.Text = "Click";
            }

        }
    }
}
