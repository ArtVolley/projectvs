﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labMoveImage
{
    public partial class Form1 : Form
    {
        private readonly Bitmap b;
        private Point curPoint;
        private Point startPoint;

        public Form1()
        {
            InitializeComponent();

            b = new Bitmap(Properties.Resources.icon_video);

            this.DoubleBuffered = true;
            this.Paint += (s, e) => e.Graphics.DrawImage(b, curPoint);
            this.MouseDown += (s, e) => startPoint = e.Location;
            this.MouseMove += Form1_MouseMove;

        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                curPoint.X += e.X - startPoint.X;
                curPoint.Y += e.Y - startPoint.Y;
                startPoint = e.Location;
                this.Invalidate();
            }
        }
    }
}


//  HW:

//  изменять размер по колесику под курсором (ограничения)
//  
