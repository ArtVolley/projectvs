﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labImageRotate
{
    public partial class Form1 : Form
    {
        private Bitmap hoami;
        public Form1()
        {
            InitializeComponent();

            hoami = new Bitmap(Properties.Resources.hoami);

            pictureBox1.Paint += PictureBox1_Paint;
            button1.Click += (s, e) => { hoami.RotateFlip(RotateFlipType.RotateNoneFlipX); pictureBox1.Invalidate();  };
            button2.Click += (s, e) => { hoami.RotateFlip(RotateFlipType.RotateNoneFlipY); pictureBox1.Invalidate();  };
            trackBar1.ValueChanged += (s, e) => pictureBox1.Invalidate();

            // hoami.rotateFlip
            // pictureBox1.Image.rotateFlip
        }

        private void PictureBox1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.TranslateTransform(hoami.Width / 2, hoami.Height / 2);
            e.Graphics.RotateTransform(trackBar1.Value);
            e.Graphics.DrawImage(hoami, -hoami.Width/2, -hoami.Height/2);

            e.Graphics.RotateTransform(-trackBar1.Value);
            e.Graphics.TranslateTransform(hoami.Width / 2, hoami.Height / 2);
            e.Graphics.DrawImage(hoami, -hoami.Width / 2, -hoami.Height / 2);
        }
    }
}
