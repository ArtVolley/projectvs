﻿namespace labMathTrain
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.laWrong = new System.Windows.Forms.Label();
            this.laCorrect = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.buNo = new System.Windows.Forms.Button();
            this.buYes = new System.Windows.Forms.Button();
            this.justText = new System.Windows.Forms.Label();
            this.laCode = new System.Windows.Forms.Label();
            this.laLvl = new System.Windows.Forms.Label();
            this.buLvlUp = new System.Windows.Forms.Button();
            this.buLvlDown = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.laWrong, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.laCorrect, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(776, 100);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // laWrong
            // 
            this.laWrong.AutoSize = true;
            this.laWrong.BackColor = System.Drawing.Color.LightPink;
            this.laWrong.Dock = System.Windows.Forms.DockStyle.Fill;
            this.laWrong.Font = new System.Drawing.Font("Segoe UI Light", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.laWrong.Location = new System.Drawing.Point(388, 0);
            this.laWrong.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.laWrong.Name = "laWrong";
            this.laWrong.Size = new System.Drawing.Size(385, 100);
            this.laWrong.TabIndex = 2;
            this.laWrong.Text = "НЕВЕРНО = 0";
            this.laWrong.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // laCorrect
            // 
            this.laCorrect.AutoSize = true;
            this.laCorrect.BackColor = System.Drawing.Color.LightGreen;
            this.laCorrect.Dock = System.Windows.Forms.DockStyle.Fill;
            this.laCorrect.Font = new System.Drawing.Font("Segoe UI Light", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.laCorrect.Location = new System.Drawing.Point(3, 0);
            this.laCorrect.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.laCorrect.Name = "laCorrect";
            this.laCorrect.Size = new System.Drawing.Size(385, 100);
            this.laCorrect.TabIndex = 2;
            this.laCorrect.Text = "ВЕРНО = 0";
            this.laCorrect.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.buNo, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.buYes, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(12, 338);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(776, 100);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // buNo
            // 
            this.buNo.BackColor = System.Drawing.Color.LightPink;
            this.buNo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buNo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buNo.Font = new System.Drawing.Font("Segoe UI Light", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buNo.Location = new System.Drawing.Point(391, 3);
            this.buNo.Name = "buNo";
            this.buNo.Size = new System.Drawing.Size(382, 94);
            this.buNo.TabIndex = 0;
            this.buNo.Text = "НЕТ";
            this.buNo.UseVisualStyleBackColor = false;
            // 
            // buYes
            // 
            this.buYes.BackColor = System.Drawing.Color.LightGreen;
            this.buYes.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buYes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buYes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buYes.Font = new System.Drawing.Font("Segoe UI Light", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buYes.Location = new System.Drawing.Point(3, 3);
            this.buYes.Name = "buYes";
            this.buYes.Size = new System.Drawing.Size(382, 94);
            this.buYes.TabIndex = 0;
            this.buYes.Text = "ДА";
            this.buYes.UseVisualStyleBackColor = false;
            // 
            // justText
            // 
            this.justText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.justText.Font = new System.Drawing.Font("Segoe UI Light", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.justText.Location = new System.Drawing.Point(15, 310);
            this.justText.Name = "justText";
            this.justText.Size = new System.Drawing.Size(770, 25);
            this.justText.TabIndex = 3;
            this.justText.Text = "Верно?";
            this.justText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // laCode
            // 
            this.laCode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.laCode.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.laCode.Font = new System.Drawing.Font("Segoe UI Light", 69.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.laCode.Location = new System.Drawing.Point(15, 177);
            this.laCode.Name = "laCode";
            this.laCode.Size = new System.Drawing.Size(773, 121);
            this.laCode.TabIndex = 2;
            this.laCode.Text = "9+9=2";
            this.laCode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // laLvl
            // 
            this.laLvl.AutoSize = true;
            this.laLvl.Font = new System.Drawing.Font("Segoe UI Light", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.laLvl.Location = new System.Drawing.Point(285, 115);
            this.laLvl.Name = "laLvl";
            this.laLvl.Size = new System.Drawing.Size(114, 30);
            this.laLvl.TabIndex = 4;
            this.laLvl.Text = " Уровень: 1";
            // 
            // buLvlUp
            // 
            this.buLvlUp.BackColor = System.Drawing.Color.LightGreen;
            this.buLvlUp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buLvlUp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buLvlUp.Location = new System.Drawing.Point(416, 119);
            this.buLvlUp.Name = "buLvlUp";
            this.buLvlUp.Size = new System.Drawing.Size(40, 27);
            this.buLvlUp.TabIndex = 5;
            this.buLvlUp.Text = "/\\";
            this.buLvlUp.UseVisualStyleBackColor = false;
            // 
            // buLvlDown
            // 
            this.buLvlDown.BackColor = System.Drawing.Color.LightPink;
            this.buLvlDown.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buLvlDown.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buLvlDown.Location = new System.Drawing.Point(462, 119);
            this.buLvlDown.Name = "buLvlDown";
            this.buLvlDown.Size = new System.Drawing.Size(40, 27);
            this.buLvlDown.TabIndex = 6;
            this.buLvlDown.Text = "\\/";
            this.buLvlDown.UseVisualStyleBackColor = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buLvlDown);
            this.Controls.Add(this.buLvlUp);
            this.Controls.Add(this.laLvl);
            this.Controls.Add(this.justText);
            this.Controls.Add(this.laCode);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MinimumSize = new System.Drawing.Size(816, 489);
            this.Name = "Form1";
            this.Text = "УСТНЫЙ СЧЁТ";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label laWrong;
        private System.Windows.Forms.Label laCorrect;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button buNo;
        private System.Windows.Forms.Button buYes;
        private System.Windows.Forms.Label justText;
        private System.Windows.Forms.Label laCode;
        private System.Windows.Forms.Label laLvl;
        private System.Windows.Forms.Button buLvlUp;
        private System.Windows.Forms.Button buLvlDown;
    }
}

