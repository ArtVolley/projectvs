﻿using System;
using System.Runtime.Serialization;

namespace labMathTrain
{
    internal class Game
    {
        public Game()
        {
        }
        public int countCorrect { get; private set; }
        public int countWrong { get; private set; }
        public string codeText { get; private set; }
        public int lvl { get; set; } = 1;

        private bool AnswerCorrect;

        public event EventHandler Change;


        public void doReset()
        {
            countCorrect = 0;
            countWrong = 0;
            doContinue();
        }

        private void doContinue()
        {
            Random rnd = new Random();
            int rndTrue = rnd.Next(2);  // рандомность выпадения истинности и лжи
            AnswerCorrect = true;

            int result = this.result();  // высчитываем истинное
            if (rndTrue == 0)
            {
                AnswerCorrect = false;
                result = result + (rnd.Next(15) + 1) * (rnd.Next(2) == 1 ? -1 : 1);
            }

            codeText += $"{result}";

            Change?.Invoke(this, EventArgs.Empty);
        }

        private int result()
        {
            Random rnd = new Random();
            int rndSymbol = rnd.Next(4);  // рандомность выпадения знаков
            int x = rnd.Next(lvl*10);
            int y = rnd.Next(lvl*10);

            int hj = lvl;

            switch (rndSymbol)
            {
                case 0:  // +
                    codeText = $"{x} + {y} = ";
                    return x + y;
                    
                case 1:  // -
                    if (x <= y)  // чтобы не было ответа с минусом
                    {
                        return result();
                    }
                    else
                    {
                        codeText = $"{x} - {y} = ";
                        return x - y;
                    }
                    
                case 2:  // /
                    if ((x <= y) || (y == 0))  // деление на ноль и дробные меньше 1
                    {
                        return result();
                    }
                    else
                    {
                        codeText = $"{x} / {y} = ";
                        return x / y;
                    }
                    
                case 3:  // *
                    codeText = $"{x} * {y} = ";
                    return x * y;

                default:
                    return 0;
            }
            
        }

        public void doAnswer(bool v)
        {
            if (v == AnswerCorrect)
            {
                countCorrect++;
            }
            else
            {
                countWrong++;
            }
            doContinue();
        }
    }
}