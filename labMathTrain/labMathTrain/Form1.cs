﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labMathTrain
{
    public partial class Form1 : Form
    {
        private Game g;
        private int lvl = 1;
        public Form1()
        {
            InitializeComponent();

           
        
            g = new Game();
            g.Change += G_Change;
            g.doReset();
        

            buLvlUp.Click += BuLvlUp_Click;
            buLvlDown.Click += BuLvlDown_Click;
            buYes.Click += (s, e) => g.doAnswer(true);
            buNo.Click += (s, e) => g.doAnswer(false);
        }

        

        private void BuLvlDown_Click(object sender, EventArgs e)
        {
            if (g.lvl <2)
            {
                MessageBox.Show("Думаю это и так слишком просто");
            }
            else
            {
                g = new Game();
                lvl--;
                g.lvl = lvl;
                g.Change += G_Change;
                g.doReset();
            }
            laLvl.Text = $"Уровень: {g.lvl}";
        }

        private void BuLvlUp_Click(object sender, EventArgs e)
        {
            if (g.lvl > 90)
            {
                MessageBox.Show("Это ты уже не посчитаешь");
            }
            else
            { 
                g = new Game();
                lvl++;
                g.lvl = lvl;
                int po = g.lvl;
                g.Change += G_Change;
                g.doReset();
            }
            laLvl.Text = $"Уровень: {g.lvl}";
            
        }

        private void G_Change(object sender, EventArgs e)
        {
            laCorrect.Text = $"ВЕРНО = {g.countCorrect}";
            laWrong.Text = $"НЕВЕРНО = {g.countWrong}";
            laCode.Text = g.codeText; 
        }
    }
}
