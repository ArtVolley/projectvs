﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labCards
{
    public partial class Form1 : Form
    {
        private readonly Bitmap b;
        private readonly Graphics g;
        private readonly ImageBox imageBox;
        private readonly CardPack cardPack;
        private int cardCount = 6;
        private PlayerCards pCards;

        public Form1()
        {
            InitializeComponent();


            b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            g = Graphics.FromImage(b);
            
            imageBox = new ImageBox(Properties.Resources.cards1, 4, 13);
            
            cardPack = new CardPack(imageBox.Count);

            this.DoubleBuffered = true;
            this.Paint += (s, e) => e.Graphics.DrawImage(b, 0, 0);


            this.Click += Form1_Click;
            this.KeyDown += Form1_KeyDown;

        }


        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            Random rnd = new Random();
            switch (e.KeyCode)
            {
                case Keys.T:
                    pCards = new PlayerCards(cardPack, cardCount);
                    g.Clear(SystemColors.Control);
                    g.TranslateTransform(300, 300);
                    g.RotateTransform(-65);
                    for (int i = 0; i < cardCount; i++)
                    {
                        g.TranslateTransform(10, 0);
                        g.RotateTransform(20);
                        g.DrawImage(imageBox[pCards[i]], -imageBox[pCards[i]].Width / 2, -imageBox[pCards[i]].Height);
                    }
                    this.Invalidate();
                    break;
                case Keys.R:
                    g.Clear(SystemColors.Control);
                    for (int i = 0; i < imageBox.Count; i++)
                    {
                        g.DrawImage(imageBox[cardPack[i]], rnd.Next(this.Width - 200), rnd.Next(this.Height - 200));
                    }
                    this.Invalidate();
                    break;
                default:
                    break;
            }
        }

        private void Form1_Click(object sender, EventArgs e)
        {
            Random rnd = new Random();
            g.Clear(SystemColors.Control);
            //g.DrawImage(imageBox[30], 0, 0);
        }
    }
}


//  HW





//  мышкой двигать веер