﻿using System;
using System.Linq;

namespace labCards
{
    internal class CardPack
    {
        private int Count;
        private int[] arr;
        private Random rnd = new Random();
        public CardPack(int count)
        {
            Count = count;
            CardRandom();
        }

        public int this[int index] { get { return arr[index];  } }

        public void CardSort()
        {
            arr = Enumerable.Range(0, Count).ToArray();
        }


        public void CardRandom()
        {
            arr = Enumerable.Range(0, Count).OrderBy(_ => rnd.Next()).ToArray();
        }

        public int Length()
        {
            return arr.Length;
        }

        public void RemoveLast()
        {
            arr = arr.Take(arr.Length - 1).ToArray();
            Count--;
        }

        public int Last()
        {

            return arr.LastOrDefault();
        }
    }
}