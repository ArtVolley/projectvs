﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace labCards
{
    internal class PlayerCards
    {
        private int count;
        private CardPack cardPack;
        private int[] urCards = new int[6];
        private Random rnd = new Random();


        public PlayerCards(CardPack _cardPack, int _count)
        {
            cardPack = _cardPack;
            count = _count;
            CardRandom();
        }

        public int this[int index] { get { return urCards[index]; } }

        public void CardRandom()
        {
            int[] cPack = new int[cardPack.Length()];
            for (int i = 0; i < cardPack.Length(); i++)
            {
                cPack[i] = cardPack[i];
            }
            for (int i = 0; i < count; i++)
            {
                urCards[i] = cardPack.Last();
                cardPack.RemoveLast();
            }
        }
    }
}