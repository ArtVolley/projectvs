﻿using System;
using System.ComponentModel.Design;
using System.Drawing;

namespace labCards
{
    internal class ImageBox
    {
        private Bitmap[] images;

        public int Rows { get; }
        public int Cols { get; }
        public int Count { get; }


        public ImageBox(Bitmap image, int rows, int cols): this(image, rows, cols, rows * cols) {   }

        public ImageBox(Bitmap image, int rows, int cols, int count) 
        {
            Rows = rows;
            Cols = cols;
            Count = count;
            LoadImages(image);
        }

        public Bitmap this[int index]   {get    {   return images[index];   }   }

        private void LoadImages(Bitmap image)
        {
            images = new Bitmap[Count];
            double w = Convert.ToDouble(image.Width) / Convert.ToDouble(Cols);
            double h = Convert.ToDouble(image.Height) / Convert.ToDouble(Rows);
            var n = 0;
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols && n < Count; j++, n++)
                {
                    images[n] = new Bitmap(Convert.ToInt32(w), Convert.ToInt32(h));
                    var g = Graphics.FromImage(images[n]);
                    g.DrawImage(image, 0, 0,
                        new Rectangle(Convert.ToInt32(j * w), Convert.ToInt32(i * h), Convert.ToInt32(w), Convert.ToInt32(h)),
                        GraphicsUnit.Pixel);
                    g.Dispose();
                }
            }
        }
    }
}