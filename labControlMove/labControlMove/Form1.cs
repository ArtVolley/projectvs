﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labControlMove
{
    public partial class Form1 : Form
    {
        private Point startPoint;

        public Form1()
        {
            InitializeComponent();

            panel1.MouseDown += All_MouseDown;
            panel2.MouseDown += All_MouseDown;
            pictureBox1.MouseDown += All_MouseDown;

            panel1.MouseMove += All_MouseMove;
            panel2.MouseMove += All_MouseMove;
            pictureBox1.MouseMove += All_MouseMove;

            panel1.MouseWheel += All_MouseWheel;
            panel2.MouseWheel += All_MouseWheel;
            pictureBox1.MouseWheel += All_MouseWheel;
        }

        private void All_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Point finishPoint = new Point(Cursor.Position.X - startPoint.X, Cursor.Position.Y - startPoint.Y);
                if (sender is Control x)
                {
                    x.Location = PointToClient(finishPoint);
                }
            }
        }

        private void All_MouseDown(object sender, MouseEventArgs e)
        {
            startPoint = e.Location;
            if (sender is Control x)
            {
                x.BringToFront();
            }
        } 
        private void All_MouseWheel(object sender, MouseEventArgs e)
        {
            int delta = e.Delta > 0 ? -2 : 2;
            if (sender is Control x)
            {
                x.Width += delta * 2;
                x.Height += delta * 2;
                x.Location = new Point(x.Location.X - delta, x.Location.Y - delta);
            }
        }
    }
}
