﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labEvent
{
    public partial class Fm : Form
    {
        public Fm()
        {
            InitializeComponent();
            // (2)
            button2.Click += Button2_Click;
            // (3)
            button3.Click += delegate
            {
                MessageBox.Show("Способ 3");
            };
            // (4)
            button4.Click += (s, e) => MessageBox.Show("Способ 4");
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Способ 2");
        }

        // (1)
        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Способ 1");
        }
    }
}
