﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labFileExplorer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            // CurDir = "D:/";
            CurDir = Directory.GetCurrentDirectory();
            buBack.Click += BuBack_Click;
            buForward.Click += BuForward_Click;
            buUp.Click += BuUp_Click; ;
            edDir.KeyDown += EdDir_KeyDown;
            buDirSelect.Click += BuDirSelect_Click;

            lv.KeyDown += Form1_KeyDown;

            miViewLargeIcon.Click += (s, e) => lv.View = View.LargeIcon;
            miViewSmallIcon.Click += (s, e) => lv.View = View.SmallIcon;
            miViewList.Click += (s, e) => lv.View = View.List;
            miViewDetails.Click += (s, e) => lv.View = View.Details;
            miViewTile.Click += (s, e) => lv.View = View.Tile;
            
            lv.ItemSelectionChanged += (s, e) => SelItem = Path.Combine(CurDir, e.Item.Text);
            lv.DoubleClick += (s, e) => LoadDir(SelItem);

            // способ 1
            //var c1 = new ColumnHeader();
            //c1.Text = "Имя";
            //c1.Width = 400;
            //lv.Columns.Add(c1);
            //способ 2
            //lv.Columns.Add(new ColumnHeader() { Text = "Имя", Width = 400 });
            //способ 3
            lv.Columns.Add("Имя", 400);
            lv.Columns.Add("Дата изменения", 150);
            lv.Columns.Add("Тип", 100);
            lv.Columns.Add("Размер", 150);
            LoadDir(CurDir);

            Text += " :Drivers= " + string.Join(" ", Directory.GetLogicalDrives());
            //--исправить ошибку в buUp
            //--исправить ошибку при вводе путя ( пользователь может ввести что то не то) (сделать проверку)
            //--если дабл клик по файлу, то показать его message.box путь
            //--добавить buBack.Click и тд с помощью стека
            //--у mp3 файлов своя иконка, у док, видео и тд тоже (item.Extension - расширение)
            //--указать размер в байтах, кб, мб
            //--указать тип файла (расширение)
            //добавить еще одну панель и на ней динамически создать кнопки с дисками C, D, E и тд, а рядом нарисовать картинки
            //--при нажатии на backspace переход на уровень выше buUp
            //по пробелу посчитать размер папки и показать в столбце размер
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Back:
                    BuUp_Click(sender, e);
                    break;
                case Keys.Space:
                    long weight = 0;
                    DirectoryInfo directoryInfo = new DirectoryInfo(CurDir);
                    MessageBox.Show($"{findWeight(findDirWeight(directoryInfo, weight))}");
                    break;
                default:                  
                    break;
            }
        }

        private void BuForward_Click(object sender, EventArgs e)
        {
            if (NextDir.Count > 0)
            {
                LoadDir(NextDir.Pop().ToString());
            }
        }

        private void BuBack_Click(object sender, EventArgs e)
        {
            if (LastDir.Count > 0)
            {
                NextDir.Push(CurDir);
                LoadDir(LastDir.Pop().ToString());
                LastDir.Pop();
            }
        }

        private void BuUp_Click(object sender, EventArgs e)
        {
            if (Directory.GetParent(CurDir) != null)
            LoadDir(Directory.GetParent(CurDir).ToString());
        }

        private void BuDirSelect_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
                LoadDir(dialog.SelectedPath);
        }

        private void EdDir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                DirectoryInfo edDirInfo = new DirectoryInfo(edDir.Text);
                if (edDirInfo.Exists)
                    LoadDir(edDir.Text);
                else
                    MessageBox.Show("Вводите адрес");
            }
        }

        private string findWeight(long lenght)
        {
            if (lenght / 1024 > 0)
            {
                if (lenght / 1024 / 1024 > 0)
                {
                    if (lenght / 1024 / 1024 / 1024 > 0)
                    {
                        return (lenght / 1024 / 1024 / 1024).ToString() + " ГБ";
                    }
                    else
                        return (lenght / 1024 / 1024).ToString() + " МБ";
                }
                else
                    return (lenght / 1024).ToString() + " КБ";
            }
            else
                return lenght.ToString() + " байт";
        }

        private long findDirWeight(DirectoryInfo directoryInfo, long weight)
        {
            foreach(var item in directoryInfo.GetFiles())
            {
                weight += item.Length;
            }
            foreach(var item in directoryInfo.GetDirectories())
            {
                weight += findDirWeight(item, weight);
            }
            return weight;
        }

        private void LoadDir(string newDir)
        {
            if (File.Exists(newDir))
            {
                MessageBox.Show($"{newDir}");
            }
            else
            {
                DirectoryInfo directoryInfo = new DirectoryInfo(newDir);
                LastDir.Push(CurDir);
                lv.BeginUpdate();
                lv.Items.Clear();
                foreach (var item in directoryInfo.GetDirectories())
                {
                    //способ 1
                    //tems.Add(item.Name, 1);
                    //способ 2
                    lv.Items.Add(new ListViewItem(
                        new string[] {item.Name, item.LastWriteTime.ToString(), "Папка", ""},
                        1));
                }
                foreach (var item in directoryInfo.GetFiles())
                {
                    //1
                    //lv.Items.Add(item.Name, 0);
                    //2
                    if ((item.Extension == ".doc") || (item.Extension == ".txt") || (item.Extension == ".pdf"))
                    {
                         lv.Items.Add(new ListViewItem(
                             new string[] { item.Name, item.LastWriteTime.ToString(), item.Extension,
                                    findWeight(item.Length)},
                             2));
                    }
                    if (item.Extension == ".mp3")
                    {
                        lv.Items.Add(new ListViewItem(
                            new string[] { item.Name, item.LastWriteTime.ToString(), item.Extension,
                                    findWeight(item.Length)},
                            3));
                    }
                    if ((item.Extension == ".img") || (item.Extension == ".png") || (item.Extension == ".jpg"))
                    {
                        lv.Items.Add(new ListViewItem(
                            new string[] { item.Name, item.LastWriteTime.ToString(), item.Extension,
                                    findWeight(item.Length)},
                            4));
                    }
                    if ((item.Extension == ".mp4") || (item.Extension == ".avi"))
                    {
                        lv.Items.Add(new ListViewItem(
                            new string[] { item.Name, item.LastWriteTime.ToString(), item.Extension,
                                    findWeight(item.Length)},
                            5));
                    }
                    else
                    {
                        lv.Items.Add(new ListViewItem(
                            new string[] { item.Name, item.LastWriteTime.ToString(), item.Extension, 
                                findWeight(item.Length)},
                            0));
                    }
                }
                lv.EndUpdate();
                edDir.Text = newDir;
                CurDir = newDir;
            }
        }

        public string CurDir { get; private set; }
        public string SelItem { get; private set; }
        public Stack LastDir = new Stack();
        public Stack NextDir = new Stack();
    }
}
