﻿using System;
using System.Linq;

namespace labLINQext
{
    class Program
    {
        static void Main(string[] args)
        {
            var arr = new string[] { "Сочи", "Москва", "Орел", "Тула", "Сумы", "Севастополь", "Ялта", "Смоленск", };
            Console.WriteLine(string.Join(Environment.NewLine, arr));
            Console.WriteLine("---");

            var x1 = arr.Where(v => v.StartsWith("С")).Where(v => v.Length < 5).ToArray();
            Console.WriteLine(string.Join(Environment.NewLine, x1));
            Console.WriteLine("---");

            var x2 = arr.OrderBy(v => v).Select(v => $"[{v}] - {v.Contains('а')} - {v.Length}").ToArray();
            Console.WriteLine(string.Join(Environment.NewLine, x2));
            Console.WriteLine("---");
        }
    }
}
