﻿using System;
using System.Collections;

namespace LabStack
{
    class Program
    {
        static void Main(string[] args)
        {
            Stack x = new Stack();
            x.Push("hello");
            x.Push(55);
            x.Push("hello");

            Console.WriteLine(x.Peek());
            Console.WriteLine("--");

            Console.WriteLine(x.Pop());
            Console.WriteLine(x.Pop());
            Console.WriteLine(x.Pop());

            //try
            //{
            //    Console.WriteLine(x.Pop());
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine($"чтото не так. Ошибка = {ex.Message}");
            //}

        }
    }
}
