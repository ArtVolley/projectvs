﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labSearchCity
{
    public partial class Form1 : Form
    {
        private string[] arr;

        public Form1()
        {
            InitializeComponent();

            arr = Properties.Resources.Cities.Split('\n');
            edSearch.TextChanged += EdSearch_TextChanged;
        }

        private void EdSearch_TextChanged(object sender, EventArgs e)
        {
            this.Text = $"{Application.ProductName}  :  {edSearch.Text}";
            var r = arr.Where(v => v.ToUpper().Contains(edSearch.Text.ToUpper()))
                .Take(50)
                .Select(v => $"{v} - {v.ToUpper()}")
                .OrderBy(v => v)            
                .ToArray();

            edResult.Text = string.Join(Environment.NewLine, r);
            this.Text += $" : Count = {r.Count()}";

        }
    }
}
