﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labImageScrollHorz
{
    public partial class Form1 : Form
    {
        private readonly Bitmap bBG;
        private readonly Bitmap bG;
        private readonly Graphics gBG;
        private readonly Graphics gG;
        private Bitmap imgBG;
        private Bitmap imgG;
        private Point startPoint;
        private int deltaXbg;
        private int deltaXg;
        private int speedBG = 2;
        private int speedG = 5;


        public Form1()
        {
            InitializeComponent();

            // 206

            imgBG = Properties.Resources.bg3;
            imgG = Properties.Resources.bg3_1;
            this.Height = imgBG.Height + 39;
            bBG = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            bG = new Bitmap(Screen.PrimaryScreen.Bounds.Width, imgG.Height);
            gBG = Graphics.FromImage(bBG);
            gG = Graphics.FromImage(bG);

            this.DoubleBuffered = true;
            this.Paint += (s, e) => { UpdateBG(); e.Graphics.DrawImage(bBG, 0, 0); };
            this.Paint += (s, e) => { UpdateG(); e.Graphics.DrawImage(bG, 0, 206); };
            this.MouseDown += (s, e) => startPoint = e.Location;
            this.MouseMove += Form_MouseMove;
            this.KeyDown += Form1_KeyDown;
            this.KeyUp += Form1_KeyUp;
        }


        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            speedBG = 2;
            speedG = 5;
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (speedBG < imgBG.Width )
            {
                speedBG ++;
            }
            if (speedG < imgG.Width)
            {
                speedG += 2;
            }
            switch (e.KeyCode)
            {
                case Keys.Left:
                    UpdateDeltaXbg(speedBG);
                    UpdateDeltaXg(speedG);
                    break;
                case Keys.Right:
                    UpdateDeltaXbg(-speedBG);
                    UpdateDeltaXg(-speedG);
                    break;
            }
            this.Invalidate();
        }

        private void Form_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                UpdateDeltaXbg(e.X - startPoint.X);
                UpdateDeltaXg((e.X - startPoint.X) + 1);
                startPoint = e.Location;
                this.Invalidate();
            }
        }

        private void UpdateDeltaXbg(int v)
        {
            this.Text = $"{deltaXbg} : {v}";
            deltaXbg += v;
            if (deltaXbg > 0)
                deltaXbg -= imgBG.Width;
            else
                if (deltaXbg < -imgBG.Width)
                deltaXbg += imgBG.Width;
        }

        private void UpdateDeltaXg(int v)
        {
            deltaXg += v;
            if (deltaXg > 0)
                deltaXg -= imgG.Width;
            else
                if (deltaXg < -imgG.Width)
                deltaXg += imgG.Width;
        }

        private void UpdateBG()
        {
            int k = this.Width / imgBG.Width + 2;
            gBG.Clear(SystemColors.Control);
            for (int i = 0; i < k; i++)
            {
                gBG.DrawImage(imgBG,deltaXbg +  i * imgBG.Width, 0);
            }
        }

        private void UpdateG()
        {
            int k = this.Width / imgG.Width + 2;
            gG.Clear(SystemColors.Control);
            for (int i = 0; i < k; i++)
            {
                gG.DrawImage(imgG, deltaXg + i * imgG.Width, 0);
            }
        }
    }
}
