using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Racing
{
    static class Program
    {
        
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]

        static void Main()
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            object[,] cars = new object[3, 4]
            {   {"classic", true, true, 0},
                {"green", false, false, 50},
                {"neon", false, false, 100}   };
            // 0 - id ������, 1 - ������� ��� ���, 2 - ������� ��� ���, 3 - ����
            Application.Run(new Menu(0, 1000, cars));
        }
    }
}
