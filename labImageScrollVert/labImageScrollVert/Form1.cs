﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labImageScrollVert
{
    public partial class Form1 : Form
    {
        private Bitmap imgBG;
        
        private int deltaY;
        
        
        private System.Timers.Timer aTimer;
        private readonly Bitmap bBG;
        private readonly Graphics gBG;
        private int bgHeight;

        public Form1()
        {
            InitializeComponent();


            imgBG = Properties.Resources.road;
            
            this.Width = imgBG.Width;
            bBG = new Bitmap(imgBG.Width, Screen.PrimaryScreen.Bounds.Height);
            
            gBG = Graphics.FromImage(bBG);
            bgHeight = imgBG.Height;
            



            this.DoubleBuffered = true;
            
            this.Paint += (s, e) => { UpdateBG(); e.Graphics.DrawImage(bBG, (this.Width - bBG.Width)/2 - 9, 0); };
            this.Resize += (s, e) => { Invalidate(); };
            this.BackColor = Color.FromArgb(102, 102, 102);
            this.KeyDown += Form1_KeyDown;
            pxCar.MouseMove += PxCar_MouseMove;
        }


        private void PxCar_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (sender is PictureBox x)
                {
                    Point loc = x.Location;
                    loc.X += e.Location.X - (x.Width / 2);
                    loc.Y += e.Location.Y - (x.Height / 2);
                    x.Location = loc;
                    this.Invalidate();
                }
            }
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.S:
                    aTimer = new System.Timers.Timer();
                    aTimer.Interval = 1;
                    aTimer.AutoReset = true;
                    aTimer.Enabled = true;
                    aTimer.Elapsed += ATimer_Elapsed;
                    break;
                case Keys.Up:
                    UpdateDeltaY(10);
                    this.Invalidate();
                    break;
                case Keys.Down:
                    UpdateDeltaY(-10);
                    this.Invalidate();
                    break;
                case Keys.Left:
                    Point loc = pxCar.Location;
                    loc.X -= 5;
                    pxCar.Location = loc;
                    pxCar.Invalidate();
                    break;
                case Keys.Right:
                    loc = pxCar.Location;
                    loc.X += 5;
                    pxCar.Location = loc;
                    pxCar.Invalidate();
                    break;

            }
        }

        private void ATimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            UpdateDeltaY(5);
            this.Invalidate();
        }

        private void UpdateDeltaY(int v)
        {
            //this.Text = $"{deltaY} : {v}";
            deltaY += v;
            if (deltaY > 0)
            {
                deltaY -= bgHeight;

            }
            else
            {
                if (deltaY < -bgHeight)
                {
                    deltaY += bgHeight;

                }
            }
        }

        private void UpdateBG()
        {
            gBG.Clear(SystemColors.Control);
            for (int i = 0; i < 3; i++)
            {
                gBG.DrawImage(imgBG, 0, deltaY + i * imgBG.Height);
            }
        }
    }
}


//  HW:

//  фон вертикальный по центру (дорога)
//  персонаж перемещается клава мышка