﻿using System;
using System.Collections.Generic;

namespace LabQueueT
{
    class Program
    {
        static void Main(string[] args)
        {
            Queue<int> x = new Queue<int>();
            x.Enqueue(2);
            x.Enqueue(3);
            x.Enqueue(4);
            x.Enqueue(5);

            Console.WriteLine(x.Peek());

            Console.WriteLine("---");

            while (x.Count > 0)
            {
                Console.WriteLine(x.Dequeue());
            }

        }
    }
}
