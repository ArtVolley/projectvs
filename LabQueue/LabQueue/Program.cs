﻿using System;
using System.Collections;

namespace LabQueue
{
    class Program
    {
        static void Main(string[] args)
        {
            Queue x = new Queue();
            x.Enqueue(6);
            x.Enqueue("hello");
            x.Enqueue(77);
            x.Enqueue(new Object());

            Console.WriteLine(x.Peek());

            Console.WriteLine("---");

            while (x.Count > 0)
            {
                Console.WriteLine(x.Dequeue());
            }

        }
    }
}
