﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labZoomImage
{
    public partial class Form1 : Form
    {
        private Point startPoint;

        public int zoomDelta { get; private set; } = 50;

        public Form1()
        {
            InitializeComponent();

            pxImage.MouseMove += PxImage_MouseMove;
            pxImage.MouseWheel += PxImage_MouseWheel;
            this.KeyDown += Form_KeyDown;


            //  rounded corners
            System.Drawing.Drawing2D.GraphicsPath gp = new System.Drawing.Drawing2D.GraphicsPath();
            int d = pxZoom.Width;
            Rectangle r = new Rectangle(0, 0, pxZoom.Width, pxZoom.Height);
            gp.AddArc(r.X, r.Y, d, d, 180, 90);
            gp.AddArc(r.X + r.Width - d, r.Y, d, d, 270, 90);
            gp.AddArc(r.X + r.Width - d, r.Y + r.Height - d, d, d, 0, 90);
            gp.AddArc(r.X, r.Y + r.Height - d, d, d, 90, 90);
            pxZoom.Region = new Region(gp);


            pxZoom.Paint += PxZoom_Paint;
        }

        private void Form_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.N) && (e.Control))
            {
                OpenFileDialog dialog = new OpenFileDialog();
                dialog.Filter = "Image Files(*.BMP; *.JPG; *.GIF)| *.BMP; *.JPG; *.GIF | All files(*.*) | *.*";
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    pxImage.Image = Bitmap.FromFile(dialog.FileName);
                    pxImage.Invalidate();
                }
            }
        }

        private void PxImage_MouseWheel(object sender, MouseEventArgs e)
        {
            zoomDelta += e.Delta > 0 ? -2 : 2;
            pxZoom.Invalidate();

            if (zoomDelta > 100)
            {
                MessageBox.Show("Забыл? Это приложение ZOOM!\nОтдалять некуда уже");

                zoomDelta = 99;
            }
            if (zoomDelta < 0)
            {
                MessageBox.Show("Больше пикселей ничего не рассмотришь");

                zoomDelta = 1;
            }
        }

        private void PxZoom_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImage(pxImage.Image,
                new Rectangle(0, 0, pxZoom.Width, pxZoom.Height),
                new Rectangle(startPoint.X - zoomDelta, 
                startPoint.Y - (zoomDelta * pxZoom.Height/ pxZoom.Width),
                zoomDelta * 2,
                zoomDelta * pxZoom.Height / pxZoom.Width * 2),
                GraphicsUnit.Pixel
                );
        }

        private void PxImage_MouseMove(object sender, MouseEventArgs e)
        {
            Text = $"{Application.ProductName} :   ({e.X} , {e.Y})";
            if (pxImage.SizeMode == PictureBoxSizeMode.Zoom)
            {
                double W = pxImage.Width;
                double H = pxImage.Height;
                double imgW = pxImage.Image.Width;
                double imgH = pxImage.Image.Height;
                double k = ((W / imgW) < (H / imgH)) ? W / imgW : H / imgH;
                startPoint.X = Convert.ToInt32((e.X - (W - imgW * k) / 2) / k);
                startPoint.Y = Convert.ToInt32((e.Y - (H - imgH * k) / 2) / k);
            }
            else
            {
                startPoint = e.Location;

            }
            Point l = pxZoom.Location;
            l.X = e.X + 50;
            l.Y = e.Y + 50;
            pxZoom.Location = l;
            pxZoom.Invalidate();
        }
    }
}



