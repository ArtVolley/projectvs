﻿namespace labZoomImage
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.pxImage = new System.Windows.Forms.PictureBox();
            this.pxZoom = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pxImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxZoom)).BeginInit();
            this.SuspendLayout();
            // 
            // pxImage
            // 
            this.pxImage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pxImage.Image = ((System.Drawing.Image)(resources.GetObject("pxImage.Image")));
            this.pxImage.Location = new System.Drawing.Point(12, 12);
            this.pxImage.Name = "pxImage";
            this.pxImage.Size = new System.Drawing.Size(776, 426);
            this.pxImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pxImage.TabIndex = 0;
            this.pxImage.TabStop = false;
            // 
            // pxZoom
            // 
            this.pxZoom.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pxZoom.Location = new System.Drawing.Point(556, 12);
            this.pxZoom.Name = "pxZoom";
            this.pxZoom.Size = new System.Drawing.Size(150, 150);
            this.pxZoom.TabIndex = 1;
            this.pxZoom.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.pxZoom);
            this.Controls.Add(this.pxImage);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pxImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxZoom)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pxImage;
        private System.Windows.Forms.PictureBox pxZoom;
    }
}

