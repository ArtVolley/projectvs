﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Race
{
    public partial class Menu : Form
    {
        private Game game;
        public Menu(string score)
        {
            InitializeComponent();
            laScore.Text = score;
            game = new Game();
            buStart.Click += BuStart_Click;
        }

        private void BuStart_Click(object sender, EventArgs e)
        {
            this.Hide();
            game.ShowDialog();
        }
    }
}
