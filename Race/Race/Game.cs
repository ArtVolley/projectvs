﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Race
{
    public partial class Game : Form
    {        
        private Road road;
        public int score;
        public Game()
        {
            InitializeComponent();
            Invalidate();
            this.DoubleBuffered = true;
            carPic.Image = Properties.Resources.car;
            road = new Road(this);
            this.KeyDown += FormRace_KeyDown;
        }

        private void FormRace_KeyDown(object sender, KeyEventArgs e)
        {

            switch (e.KeyCode)
            {
                case Keys.Up:
                    road.startRoad();
                    break;
                case Keys.Down:
                    road.stopRoad();
                    break;
                case Keys.Left:
                    if (road.started)
                    {
                        this.moveCarLeft(5);

                    }
                    break;
                case Keys.Right:
                    if (road.started)
                    {
                        this.moveCarRight(5);

                    }
                    break;
            }
        }

        public Point getCarLoc()
        {
            return carPic.Location;
        }

        public int getCarWidth()
        {
            return carPic.Width;
        }

        public void setScore(int s)
        {
            laScore.Text =  s.ToString();
            score = s;
        }

        public int getCarHeight()
        {
            return carPic.Height;
        }

        public void moveCarLeft(int v)
        {
            Point loc = carPic.Location;
            loc.X -= v;
            carPic.Location = loc;
            carPic.Invalidate();
        }

        public void moveCarRight(int v)
        {
            Point loc = carPic.Location;
            loc.X += v;
            carPic.Location = loc;
            carPic.Invalidate();
        }
    }
}
