﻿namespace Race
{
    partial class Game
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.carPic = new System.Windows.Forms.PictureBox();
            this.laScore = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.carPic)).BeginInit();
            this.SuspendLayout();
            // 
            // carPic
            // 
            this.carPic.Location = new System.Drawing.Point(216, 402);
            this.carPic.Name = "carPic";
            this.carPic.Size = new System.Drawing.Size(59, 101);
            this.carPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.carPic.TabIndex = 0;
            this.carPic.TabStop = false;
            // 
            // laScore
            // 
            this.laScore.AutoSize = true;
            this.laScore.BackColor = System.Drawing.Color.Black;
            this.laScore.Font = new System.Drawing.Font("Segoe Script", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.laScore.ForeColor = System.Drawing.Color.White;
            this.laScore.Location = new System.Drawing.Point(-1, 9);
            this.laScore.Name = "laScore";
            this.laScore.Size = new System.Drawing.Size(39, 44);
            this.laScore.TabIndex = 1;
            this.laScore.Text = "0";
            // 
            // Game
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(504, 661);
            this.Controls.Add(this.laScore);
            this.Controls.Add(this.carPic);
            this.MaximumSize = new System.Drawing.Size(520, 700);
            this.MinimumSize = new System.Drawing.Size(520, 700);
            this.Name = "Game";
            this.Text = "RACE";
            ((System.ComponentModel.ISupportInitialize)(this.carPic)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox carPic;
        private System.Windows.Forms.Label laScore;
    }
}

