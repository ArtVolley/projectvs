﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Race
{
    class Road
    {
        private Game game;
        private Menu menu;
        private System.Timers.Timer aTimer;
        private Rock rock;

        private Bitmap imgRoad;
        private readonly Bitmap bRoad;
        private readonly Bitmap bRock;
        private readonly Graphics gRoad;
        private readonly Graphics gRock;
        private int roadHeight;
        private int rockHeight;
        private int deltaY;
        private int vel = 7;
        public int score;
        public bool started;
        public Road(Game game)
        {
            this.game = game;
            rock = new Rock(140, 0);
            score = -1;
            started = false;

            imgRoad = Properties.Resources.road;
            
            bRoad = new Bitmap(imgRoad.Width, Screen.PrimaryScreen.Bounds.Height);
            gRoad = Graphics.FromImage(bRoad);
            roadHeight = imgRoad.Height;
            
            bRock = new Bitmap(rock.Width, rock.Height);
            gRock = Graphics.FromImage(bRock);
            rockHeight = rock.Height;

            game.Paint += (s, e) => { UpdateRoad(); e.Graphics.DrawImage(bRoad, (game.Width - bRoad.Width) / 2 - 9, 0); };
            game.Paint += (s, e) => { UpdateRock(); e.Graphics.DrawImage(bRock, rock.X, rock.Y); };
            game.Resize += (s, e) => { game.Invalidate(); };
        }

        public void startRoad()
        {
            started = true;
            aTimer = new System.Timers.Timer();
            aTimer.Interval = 1;
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
            aTimer.Elapsed += ATimer_Elapsed;
        }

        public void stopRoad()
        {
            aTimer.Stop();
            game.Invalidate();
        }

        private void ATimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            UpdateDelta(vel);
            game.Invalidate();
        }

        private void UpdateDelta(int v)
        {
            deltaY += v;
            if (deltaY > 0)
            {
                deltaY -= roadHeight;
            }
            else
            {
                if (deltaY < -roadHeight)
                {
                    deltaY += roadHeight;
                }
            }
        }

        private void UpdateRoad()
        {
            gRoad.Clear(Color.White);
            for (int i = 0; i < 3; i++)
            {
                gRoad.DrawImage(imgRoad, 0, deltaY + i * imgRoad.Height);
            }

            
                if (game.getCarLoc().X <= (game.Width - imgRoad.Width) / 2 - 9)
                {
                    stopRoad();
                    game.Hide();
                    menu = new Menu($"Your last score: {score}");
                    menu.ShowDialog();
                }
                else 
                if (game.getCarLoc().X + game.getCarWidth() >= game.Width - ((game.Width - imgRoad.Width) / 2 - 9))
            {
                    stopRoad();
                    game.Hide();
                    menu = new Menu($"Your last score: {score}");
                    menu.ShowDialog();
                }
            
        }

        private void UpdateRock()
        {
            var rnd = new Random();
            if (rock.Y > 710)
            {
                rock.X = rnd.Next((game.Width - bRoad.Width) / 2 - 9, game.Width - ((game.Width - bRoad.Width) / 2 - 9) - rock.Width);
                score++;
                game.setScore(score);
            }


            gRock.Clear(Color.White);
            rock.Y = deltaY + imgRoad.Height - rock.Height;
            gRock.DrawImage(rock.rockImg, 0, 0);



            if ((game.getCarLoc().Y <= (rock.Y + rock.Height)) && 
                (game.getCarLoc().Y + game.getCarHeight()) >= (rock.Y))
            {
                if ((game.getCarLoc().X <= (rock.X + rock.Width)) 
                    && ((game.getCarLoc().X + game.getCarWidth()) >= (rock.X + rock.Width)))
                {
                    stopRoad();
                    game.Hide();
                    menu = new Menu($"Your last score: {score}");
                    menu.ShowDialog();
                }
                else 
                if (((game.getCarLoc().X + game.getCarWidth()) >= rock.X) &&
                    (game.getCarLoc().X <= rock.X))
                {
                    stopRoad();
                    game.Hide();
                    menu = new Menu($"Your last score: {score}");
                    menu.ShowDialog();
                }
            }
        }
    }
}
