﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Race
{
    class Rock
    {
        public int X;
        public int Y;
        public int Width;
        public int Height;

        public Image rockImg;
        public Rock(int x, int y)
        {
            Y = y;
            X = x;
            rockImg = Properties.Resources.rock;
            Height = 73;
            Width = 62;
        }

    }
}
