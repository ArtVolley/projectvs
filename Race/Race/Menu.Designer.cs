﻿namespace Race
{
    partial class Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buStart = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.laScore = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buStart
            // 
            this.buStart.BackColor = System.Drawing.Color.White;
            this.buStart.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buStart.Font = new System.Drawing.Font("Segoe UI Light", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buStart.Location = new System.Drawing.Point(165, 173);
            this.buStart.Name = "buStart";
            this.buStart.Size = new System.Drawing.Size(151, 43);
            this.buStart.TabIndex = 0;
            this.buStart.Text = "PLAY GAME!";
            this.buStart.UseVisualStyleBackColor = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe Script", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(36, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(404, 57);
            this.label1.TabIndex = 1;
            this.label1.Text = "SUPER RACE 2000";
            // 
            // laScore
            // 
            this.laScore.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.laScore.BackColor = System.Drawing.Color.Black;
            this.laScore.Font = new System.Drawing.Font("Script MT Bold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.laScore.ForeColor = System.Drawing.Color.White;
            this.laScore.Location = new System.Drawing.Point(12, 115);
            this.laScore.Name = "laScore";
            this.laScore.Size = new System.Drawing.Size(458, 23);
            this.laScore.TabIndex = 2;
            this.laScore.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(482, 246);
            this.Controls.Add(this.laScore);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buStart);
            this.MaximumSize = new System.Drawing.Size(498, 285);
            this.MinimumSize = new System.Drawing.Size(498, 285);
            this.Name = "Menu";
            this.Text = "Menu";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buStart;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label laScore;
    }
}