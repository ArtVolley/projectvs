﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labButtonGrid
{
    public partial class Form1 : Form
    {
        private Button[,] bu;
        public int rows { get; private set; } = 2;
        public int cols { get; private set; } = 2;

        public Form1()
        {
            InitializeComponent();

            ResizeTable(cols, rows);
            CreateButtons(cols, rows);

            buAddCols.Click += BuAddCols_Click;
            buAddRows.Click += BuAddRows_Click;
            buRemoveCols.Click += BuRemoveCols_Click;
            buRemoveRows.Click += BuRemoveRows_Click;
            //ResizeButtons();
            //this.Resize += (s, e) => ResizeButtons();
        }

        private void BuAddCols_Click(object sender, EventArgs e)
        {
            RemoveButtons(cols, rows);
            cols++;
            ResizeTable(cols, rows);
            CreateButtons(cols, rows);
        }
        private void BuAddRows_Click(object sender, EventArgs e)
        {
            RemoveButtons(cols, rows);
            rows++;
            ResizeTable(cols, rows);
            CreateButtons(cols, rows);
        }
        private void BuRemoveCols_Click(object sender, EventArgs e)
        {
            if (cols == 1)
            {
                MessageBox.Show("Хватит удалять, лучше прибавь");
            }
            else
            {
                RemoveButtons(cols, rows);
                cols--;
                ResizeTable(cols, rows);
                CreateButtons(cols, rows);
            }
            
        }
        private void BuRemoveRows_Click(object sender, EventArgs e)
        {
            if (rows == 1)
            {
                MessageBox.Show("Хватит удалять, лучше прибавь");
            }
            else
            {
                RemoveButtons(cols, rows);
                rows--;
                ResizeTable(cols, rows);
                CreateButtons(cols, rows);
            }
        }

        //private void ResizeButtons()
        //{
        //    int cellWidth = this.ClientSize.Width / cols - 1;
        //    int cellHeight = this.ClientSize.Height / rows - 1;

        //    for (int i = 0; i < rows; i++)
        //        for (int j = 0; j < cols; j++)
        //        {

        //            bu[i, j].Width = cellWidth;
        //            bu[i, j].Height = cellHeight;
        //            bu[i, j].Location = new Point(cellWidth * j + j, cellHeight * i + i);
        //        }
        //}

        private void ResizeTable(int cols, int rows)
        {
            taGrid.ColumnCount = 0;
            taGrid.RowCount = 0;
            taGrid.ColumnCount = cols;
            taGrid.RowCount = rows;
            taGrid.ColumnStyles.Clear();
            taGrid.RowStyles.Clear();
            for (int i = 0; i < cols; i++)
            {
                taGrid.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100 / cols));
            }
            for (int i = 0; i < rows; i++)
            {
                taGrid.RowStyles.Add(new RowStyle(SizeType.Percent, 100 / rows));
            }
           
        }

        private void CreateButtons(int cols,int rows)
        {
            //int n = 1;
            bu = new Button[rows, cols];
            for (int i = 0; i < rows; i++)
                for (int j = 0; j < cols; j++)
                {
                    bu[i, j] = new Button();
                    bu[i, j].Text = $"{1 + j + i * cols}";
                    bu[i, j].TextAlign = ContentAlignment.MiddleCenter;
                    bu[i, j].Font = new Font("Segoe UI Light", 30);
                    bu[i, j].ForeColor = Color.Purple;
                    bu[i, j].FlatStyle = FlatStyle.Flat;
                    bu[i, j].Click += buAll_Click;
                    bu[i, j].MouseHover += bu_MouseHover;
                    bu[i, j].Dock = System.Windows.Forms.DockStyle.Fill;
                    Control c = table.GetControlFromPosition(1, 1);
                    c.Controls.Add(bu[i, j]);
                }
        }

        private void bu_MouseHover(object sender, EventArgs e)
        {
            if (sender is Control x)
            {
                x.Cursor = System.Windows.Forms.Cursors.Hand;
            }
        }

        private void RemoveButtons(int cols, int rows)
        {
            for (int i = 0; i < rows; i++)
                for (int j = 0; j < cols; j++)
                {
                    Control c = table.GetControlFromPosition(1, 1);
                    c.Controls.Remove(bu[i, j]);
                }
        }


        private void buAll_Click(object sender, EventArgs e)
        {
            if (sender is Control x)
                laNum.Text = $"{x.Text}";
        }
    }
}