﻿namespace labButtonGrid
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.table = new System.Windows.Forms.TableLayoutPanel();
            this.taCols = new System.Windows.Forms.TableLayoutPanel();
            this.buRemoveCols = new System.Windows.Forms.Button();
            this.buAddCols = new System.Windows.Forms.Button();
            this.taRows = new System.Windows.Forms.TableLayoutPanel();
            this.buRemoveRows = new System.Windows.Forms.Button();
            this.buAddRows = new System.Windows.Forms.Button();
            this.taGrid = new System.Windows.Forms.TableLayoutPanel();
            this.laNum = new System.Windows.Forms.Label();
            this.table.SuspendLayout();
            this.taCols.SuspendLayout();
            this.taRows.SuspendLayout();
            this.SuspendLayout();
            // 
            // table
            // 
            this.table.ColumnCount = 2;
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.table.Controls.Add(this.taCols, 1, 0);
            this.table.Controls.Add(this.taRows, 0, 1);
            this.table.Controls.Add(this.taGrid, 1, 1);
            this.table.Controls.Add(this.laNum, 0, 0);
            this.table.Dock = System.Windows.Forms.DockStyle.Fill;
            this.table.Location = new System.Drawing.Point(0, 0);
            this.table.Name = "table";
            this.table.RowCount = 2;
            this.table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.table.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.table.Size = new System.Drawing.Size(504, 506);
            this.table.TabIndex = 0;
            // 
            // taCols
            // 
            this.taCols.ColumnCount = 2;
            this.taCols.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.taCols.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.taCols.Controls.Add(this.buRemoveCols, 0, 0);
            this.taCols.Controls.Add(this.buAddCols, 1, 0);
            this.taCols.Dock = System.Windows.Forms.DockStyle.Fill;
            this.taCols.Location = new System.Drawing.Point(50, 0);
            this.taCols.Margin = new System.Windows.Forms.Padding(0);
            this.taCols.Name = "taCols";
            this.taCols.RowCount = 1;
            this.taCols.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.taCols.Size = new System.Drawing.Size(454, 50);
            this.taCols.TabIndex = 0;
            // 
            // buRemoveCols
            // 
            this.buRemoveCols.BackColor = System.Drawing.Color.Purple;
            this.buRemoveCols.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buRemoveCols.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buRemoveCols.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buRemoveCols.Font = new System.Drawing.Font("Segoe UI Black", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.buRemoveCols.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buRemoveCols.Location = new System.Drawing.Point(3, 3);
            this.buRemoveCols.Name = "buRemoveCols";
            this.buRemoveCols.Size = new System.Drawing.Size(221, 44);
            this.buRemoveCols.TabIndex = 0;
            this.buRemoveCols.Text = "-";
            this.buRemoveCols.UseVisualStyleBackColor = false;
            // 
            // buAddCols
            // 
            this.buAddCols.BackColor = System.Drawing.Color.Purple;
            this.buAddCols.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buAddCols.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buAddCols.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buAddCols.Font = new System.Drawing.Font("Segoe UI Black", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.buAddCols.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buAddCols.Location = new System.Drawing.Point(230, 3);
            this.buAddCols.Name = "buAddCols";
            this.buAddCols.Size = new System.Drawing.Size(221, 44);
            this.buAddCols.TabIndex = 1;
            this.buAddCols.Text = "+";
            this.buAddCols.UseVisualStyleBackColor = false;
            // 
            // taRows
            // 
            this.taRows.ColumnCount = 1;
            this.taRows.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.taRows.Controls.Add(this.buRemoveRows, 0, 0);
            this.taRows.Controls.Add(this.buAddRows, 0, 1);
            this.taRows.Dock = System.Windows.Forms.DockStyle.Fill;
            this.taRows.Location = new System.Drawing.Point(0, 50);
            this.taRows.Margin = new System.Windows.Forms.Padding(0);
            this.taRows.Name = "taRows";
            this.taRows.RowCount = 2;
            this.taRows.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.taRows.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.taRows.Size = new System.Drawing.Size(50, 456);
            this.taRows.TabIndex = 1;
            // 
            // buRemoveRows
            // 
            this.buRemoveRows.BackColor = System.Drawing.Color.Purple;
            this.buRemoveRows.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buRemoveRows.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buRemoveRows.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buRemoveRows.Font = new System.Drawing.Font("Segoe UI Black", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.buRemoveRows.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buRemoveRows.Location = new System.Drawing.Point(3, 3);
            this.buRemoveRows.Name = "buRemoveRows";
            this.buRemoveRows.Size = new System.Drawing.Size(44, 222);
            this.buRemoveRows.TabIndex = 0;
            this.buRemoveRows.Text = "-";
            this.buRemoveRows.UseVisualStyleBackColor = false;
            // 
            // buAddRows
            // 
            this.buAddRows.BackColor = System.Drawing.Color.Purple;
            this.buAddRows.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buAddRows.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buAddRows.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buAddRows.Font = new System.Drawing.Font("Segoe UI Black", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.buAddRows.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buAddRows.Location = new System.Drawing.Point(3, 231);
            this.buAddRows.Name = "buAddRows";
            this.buAddRows.Size = new System.Drawing.Size(44, 222);
            this.buAddRows.TabIndex = 1;
            this.buAddRows.Text = "+";
            this.buAddRows.UseVisualStyleBackColor = false;
            // 
            // taGrid
            // 
            this.taGrid.ColumnCount = 1;
            this.taGrid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.taGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.taGrid.Location = new System.Drawing.Point(50, 50);
            this.taGrid.Margin = new System.Windows.Forms.Padding(0);
            this.taGrid.Name = "taGrid";
            this.taGrid.RowCount = 1;
            this.taGrid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.taGrid.Size = new System.Drawing.Size(454, 456);
            this.taGrid.TabIndex = 2;
            // 
            // laNum
            // 
            this.laNum.AutoSize = true;
            this.laNum.BackColor = System.Drawing.SystemColors.Window;
            this.laNum.Dock = System.Windows.Forms.DockStyle.Fill;
            this.laNum.Font = new System.Drawing.Font("Segoe UI Light", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.laNum.ForeColor = System.Drawing.SystemColors.InfoText;
            this.laNum.Location = new System.Drawing.Point(0, 0);
            this.laNum.Margin = new System.Windows.Forms.Padding(0);
            this.laNum.Name = "laNum";
            this.laNum.Size = new System.Drawing.Size(50, 50);
            this.laNum.TabIndex = 3;
            this.laNum.Text = "...";
            this.laNum.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(504, 506);
            this.Controls.Add(this.table);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Margin = new System.Windows.Forms.Padding(0, 5, 0, 5);
            this.MinimumSize = new System.Drawing.Size(520, 545);
            this.Name = "Form1";
            this.Text = "ПРОСТО КНОПКИ";
            this.table.ResumeLayout(false);
            this.table.PerformLayout();
            this.taCols.ResumeLayout(false);
            this.taRows.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel table;
        private System.Windows.Forms.TableLayoutPanel taCols;
        private System.Windows.Forms.TableLayoutPanel taRows;
        
        private System.Windows.Forms.Button buAddCols;
        private System.Windows.Forms.Button buAddRows;
        private System.Windows.Forms.TableLayoutPanel taGrid;
        private System.Windows.Forms.Button buRemoveCols;
        
        private System.Windows.Forms.Button buRemoveRows;
        private System.Windows.Forms.Label laNum;
    }
}

