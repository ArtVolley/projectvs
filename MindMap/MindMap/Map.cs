﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MindMap
{
    public partial class Map : Form
    {
        private Todos todo;
        private object[,] todos;
        private int lvlMax = 15;
        private int distX = 200;
        private int distY = 30;
        private int[] newDist;
        private bool isMouseDown;

        //MessageBox.Show($"{}");

        public Map()
        {
            InitializeComponent();

            todo = new Todos();
            todos = todo.getAll();
            CreateTextboxes(todos);
            
            this.Paint += Map_Paint;
        }

        // ---------------------------------

        private void CreateTextboxes(Object[,] todos) // создание начальных текстбоксов 
        {
            newDist = new int[lvlMax - 1];
            for (int j = 1; j < lvlMax; j++)
            {
                newDist[j - 1] = 0;
            }
            for (int i = 0; i < todos.Length / 4; i++)
            {
                TextBox txt = new TextBox();

                txt.Name = $"{todos[i, 0]}";
                txt.Text = $"{todos[i, 2]}";

                for (int j = 1; j < lvlMax; j++)
                {
                    if ((int)todos[i, 1] == j)
                    {
                        txt.Location = new Point(distX * j - 130, distY * newDist[j - 1] + 30);
                        newDist[j - 1]++;                    
                    }
                }

                TxtEvents(txt);
                TxtDesign(txt, (int)todos[i,1]);

                this.Controls.Add(txt);
            }
        }
        
        private void addTextbox(Object[] todo) // добавление одного текстбокса
        {
            TextBox txt = new TextBox();
            txt.Name = $"{todo[0]}";
            txt.Text = $"{todo[2]}";
            newDist[(int)todo[1] - 1]++;
            txt.Location = new Point(distX * (int)todo[1] - 130, distY * newDist[(int)todo[1] - 1]);

            TxtEvents(txt);
            TxtDesign(txt, (int)todo[1]);

            this.Controls.Add(txt);
        }

        // ---------------------------------

        private void Map_Paint(object sender, PaintEventArgs e) // отрисовка линий 
        {
            for (int i = 0; i < todos.Length / 4; i++)
            {
                for (int j = 0; j < todos.Length / 4; j++)
                {
                    if ((int)todos[j, 0] == (int)todos[i, 3])
                    {
                        var txtParent = this.Controls.Find($"{todos[j, 0].ToString()}", false);
                        var txtChild = this.Controls.Find($"{todos[i, 0].ToString()}", false);

                        Graphics g = e.Graphics;
                        g.SmoothingMode = SmoothingMode.AntiAlias;
                        Pen pen = new Pen(Color.White);

                        if (txtChild.Length > 0 && txtParent.Length > 0)
                        {
                            if (txtParent[0].Location.Y <= txtChild[0].Location.Y + 5 &&
                                txtParent[0].Location.Y >= txtChild[0].Location.Y - 5) // child на линии с parent  
                            {
                                g.DrawLine(pen, txtParent[0].Location.X + txtParent[0].Width,
                                                txtParent[0].Location.Y + txtParent[0].Height / 2,
                                                txtChild[0].Location.X,
                                                txtChild[0].Location.Y + txtChild[0].Height / 2);
                            }
                            else // child не на линии с parent 
                            {
                                g.DrawLine(pen, txtParent[0].Location.X + txtParent[0].Width,
                                                txtParent[0].Location.Y + txtParent[0].Height / 2,
                                                txtParent[0].Location.X + txtParent[0].Width + 20,
                                                txtParent[0].Location.Y + txtParent[0].Height / 2);

                                g.DrawLine(pen, txtParent[0].Location.X + txtParent[0].Width + 30,
                                                txtChild[0].Location.Y + txtChild[0].Height / 2,
                                                txtChild[0].Location.X,
                                                txtChild[0].Location.Y + txtChild[0].Height / 2);

                                if (txtParent[0].Location.Y <= txtChild[0].Location.Y - 5) // child ниже parent
                                {
                                    ArcLeftToDown(pen, g,
                                    txtParent[0].Location.X + txtParent[0].Width + 20,
                                    txtParent[0].Location.Y + txtParent[0].Height / 2);

                                    g.DrawLine(pen, txtParent[0].Location.X + txtParent[0].Width + 25,
                                                    txtParent[0].Location.Y + txtParent[0].Height / 2 + 5,
                                                    txtParent[0].Location.X + txtParent[0].Width + 25,
                                                    txtChild[0].Location.Y + txtChild[0].Height / 2 - 5);

                                    ArcUpToRight(pen, g,
                                        txtParent[0].Location.X + txtParent[0].Width + 25,
                                        txtChild[0].Location.Y + txtChild[0].Height / 2 - 5);
                                }
                                else if (txtParent[0].Location.Y >= txtChild[0].Location.Y + 5) // child выше parent 
                                {
                                    ArcLeftToUp(pen, g,
                                        txtParent[0].Location.X + txtParent[0].Width + 20,
                                        txtParent[0].Location.Y + txtParent[0].Height / 2);

                                    g.DrawLine(pen, txtParent[0].Location.X + txtParent[0].Width + 25,
                                                    txtParent[0].Location.Y + txtParent[0].Height / 2 - 5,
                                                    txtParent[0].Location.X + txtParent[0].Width + 25,
                                                    txtChild[0].Location.Y + txtChild[0].Height / 2 + 5);

                                    ArcDownToRight(pen, g, 
                                        txtParent[0].Location.X + txtParent[0].Width + 25, 
                                        txtChild[0].Location.Y + txtChild[0].Height / 2 - 5);

                                }
                            }

                        }

                    }
                }
            }
        }

        private void ArcLeftToDown(Pen pen, Graphics g, int x, int y)
        {
            Rectangle rect = new Rectangle(x - 5, y, 10, 10);
            g.DrawArc(pen, rect, 270, 90);
        }
        
        private void ArcLeftToUp(Pen pen, Graphics g, int x, int y)
        {
            Rectangle rect = new Rectangle(x - 5, y - 10, 10, 10);
            g.DrawArc(pen, rect, 90, -90);
        }

        private void ArcUpToRight(Pen pen, Graphics g, int x, int y)
        {
            Rectangle rect = new Rectangle(x, y - 5, 10, 10);
            g.DrawArc(pen, rect, 180, -90);
        }
        
        private void ArcDownToRight(Pen pen, Graphics g, int x, int y)
        {
            Rectangle rect = new Rectangle(x, y + 5, 10, 10);
            g.DrawArc(pen, rect, 180, 90);
        }

        // ---------------------------------

        private void TxtDesign(TextBox txt, int lvl)
        {
            if (lvl == 1)
            {
                txt.Font = new Font("Microsoft YaHei", 14, FontStyle.Bold);
                txt.BackColor = Color.FromArgb(204, 153, 204);
                txt.ForeColor = Color.White;
            }
            else if (lvl == 2)
            {
                txt.Font = new Font("Segoe UI Light", 12);
                txt.BackColor = Color.FromArgb(204, 153, 255);
                txt.ForeColor = Color.White;
            }
            else if (lvl == 3)
            {
                txt.Font = new Font("Segoe UI Light", 12);
                txt.BackColor = Color.FromArgb(153, 204, 204);
                txt.ForeColor = Color.FromArgb(51, 51, 51);
            }
            else
            {
                txt.Font = new Font("Segoe UI Light", 11);
                txt.BackColor = Color.FromArgb(255, 204, 153);
                txt.ForeColor = Color.FromArgb(51, 51, 51);
            } 


            txt.BorderStyle = BorderStyle.None;
            txt.Multiline = true;

            txt.Height = 25;
            txt.Width = 150;
            txt.Margin = new Padding(10, 10, 10, 10);
            //txt.ForeColor = Color.FromArgb(51, 51, 51);
        }

        private void TxtEvents(TextBox txt) // события на текстбокс
        {
            txt.KeyDown += Txt_KeyDown;
            txt.MouseDown += Txt_MouseDown;
            txt.MouseMove += Txt_MouseMove;
            txt.MouseUp += Txt_MouseUp;
        }

        private void Txt_MouseDown(object sender, MouseEventArgs e) // при нажатии мышки на текстбокс
        {
            if (e.Button == MouseButtons.Right)
            {
                isMouseDown = true;
            }
        }

        private void Txt_MouseMove(object sender, MouseEventArgs e) // при движении мышки на текстбоксе
        {
            if (isMouseDown)
            {
                if (sender is TextBox txt)
                {
                    for (int i = 0; i < todos.Length / 4; i++)
                    { 
                        if (Convert.ToInt32(txt.Name) == (int)todos[i, 0])
                        {

                        }
                    }

                    Point loc = txt.Location;
                    loc.X += e.Location.X - (txt.Width / 2);
                    loc.Y += e.Location.Y - (txt.Height / 2);
                    txt.Location = loc;
                    this.Invalidate();
                }
            }
        }
        // -------
        private void Txt_MouseUp(object sender, MouseEventArgs e) // при отжатии мышки на текстбоксе
        {
            if (e.Button == MouseButtons.Right)
            {
                isMouseDown = false;
            }
        }

        private void Txt_KeyDown(object sender, KeyEventArgs e) // сочетания клавиш для взаимодейсвтвия с текстбоксами
        {
            if (e.Alt)
            {
                switch (e.KeyCode)
                {
                    case Keys.Right:
                        if (sender is Control txtAdd)
                        {
                            todos = todo.getAll();
                            for (int i = 0; i < todos.Length / 4; i++)
                            {
                                if (Convert.ToInt32(txtAdd.Name) == (int)todos[i, 0])
                                {
                                    int id = (int)todos[todos.Length / 4 - 1, 0] + 1;
                                    int lvl = (int)todos[i, 1] + 1;
                                    string text = $"{id}";
                                    int parent = (int)todos[i, 0];

                                    todo.addTodo(id, lvl, text, parent);
                                    todos = todo.getAll();
                                    //MessageBox.Show($"{todos.Length /4 }");
                                    addTextbox(todo.getTodoById(id));


                                    this.Invalidate();
                                }
                            }
                        }
                        break;
                    case Keys.Delete:
                        if (sender is Control txtDel)
                        {
                            for (int i = 0; i < todos.Length / 4; i++)
                            {
                                if (Convert.ToInt32(txtDel.Name) == (int)todos[i, 0])
                                {
                                    if (Convert.ToInt32(txtDel.Name) != 1)
                                    {
                                        todo.deleteTodo(Convert.ToInt32(txtDel.Name));
                                        this.Controls.Remove(txtDel);
                                        todos = todo.getAll();
                                        for (int j = 1; j < todos.Length / 4; j++)
                                        {
                                            if (todo.haveNoParent((int)todos[j, 3]))
                                            {
                                                todo.deleteTodo((int)todos[j, 0]);
                                                var txtOrphan = this.Controls.Find($"{todos[j, 0].ToString()}", false);
                                                this.Controls.Remove(txtOrphan[0]);
                                            }
                                        }
                                        todos = todo.getAll();

                                        this.Invalidate();
                                    }
                                    else
                                    {
                                        MessageBox.Show("Вы не можете удалить главный элемент");
                                    }
                                }
                            }
                        }
                        break;
                }
            }
        } 



    }
}
