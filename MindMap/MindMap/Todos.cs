﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Windows.Forms;

namespace MindMap
{
    class Todos
    {
        private object[,] todos;
        private object[,] pastTodos;


        public Todos()
        {
            todos = new object[,]
            {
                {1, 1, " ЖИВОТНЫЕ", 0},
                {2, 2, "Звери", 1 },
                {3, 2, "Рыбы", 1 },
                {4, 2, "Грибы", 1 },
                {5, 2, "Птицы", 1 },
                {6, 2, "Насекомые", 1 },
                {7, 3, "Акула", 3 },
                {8, 3, "Кит", 3 },
                {9, 3, "Дельфин", 3 },
                {10, 3, "Орел", 5 },
                {11, 3, "Ласточка", 5 },
                {12, 3, "Пеликан", 5 },
                {13, 4, "Беркут", 10 },
                {14, 4, "Могильник", 10 },
                {15, 4, "Гриф", 10 },
                {16, 4, "Еще что-то", 10 },
            };
            // 0 - id, 1 - уровень, 2 - текст, 3 - родитель
        }

        public object[] getTodoById(int id) 
        {
            object[] _todo = new object[4];
            for (int i = 0; i < todos.Length / 4; i++)
            {
                if (id == (int)todos[i, 0])
                {
                    var _id = todos[todos.Length / 4 - 1, 0];
                    var _lvl = todos[todos.Length / 4 - 1, 1];
                    var _txt = todos[todos.Length / 4 - 1, 2];
                    var _par = todos[todos.Length / 4 - 1, 3];
                    _todo = new object[] { _id, _lvl, _txt, _par };
                }
            }
            return _todo;
        }

        public object[,] getAll()
        {
            return todos;
        }

        public void deleteTodo(int id)
        {
            int iDel = 0;
            pastTodos = todos;
            todos = new object[pastTodos.Length / 4 - 1, 4];
            for (int i = 0; i < pastTodos.Length / 4; i++)
            { 
                if ((int)pastTodos[i, 0] == id)
                {
                    iDel = i;
                }
            }

            for (int i = 0; i < iDel; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    todos[i, j] = pastTodos[i, j];
                }
            }

            for (int i = iDel; i < pastTodos.Length / 4 - 1; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    todos[i, j] = pastTodos[i + 1, j];
                }
            }
        }

        public void addTodo(int id, int lvl, string txt, int parent)
        {
            pastTodos = todos;
            todos = new object[pastTodos.Length / 4 + 1, 4];
            for (int i = 0; i < pastTodos.Length / 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    todos[i, j] = pastTodos[i, j];
                }
            }
            todos[pastTodos.Length / 4, 0] = id;
            todos[pastTodos.Length / 4, 1] = lvl;
            todos[pastTodos.Length / 4, 2] = txt;
            todos[pastTodos.Length / 4, 3] = parent;
        }

        public bool haveNoParent(int parentId)
        {
            if (parentId == 1)
            {
                return false;
            }
            else
            {
                int parents = 0;
                for (int i = 0; i < todos.Length / 4; i++)
                {
                    if ((int)todos[i, 0] == parentId)
                    {
                        parents++;
                    }
                }
                if (parents > 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
    }
}
